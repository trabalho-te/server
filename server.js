var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/messages');
var Schema = mongoose.Schema;

var messagesSchema = new Schema({
    nick: { type: String, required: true },
    content: String,
    isImage: Number,
}, { collection: 'contatos' });

var message = mongoose.model('data', messagesSchema);

var db = mongoose.connection;

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/index.html');
})
io.on('connection', function(socket) {
    console.log('one user connected ' + socket.id);
    io.to(socket.id).emit('message', { "nick": "Server", "content": "Welcome new user." , "isImage": 0})
    io.to(socket.id).emit('message', { "nick": "Server", "content": "We will update you", "isImage": 0})
    message.find({}, function(err, messages) {
        messages.forEach(function(message) {
            io.to(socket.id).emit('message', { "nick": message["nick"], "content": message["content"], "isImage": message["isImage"] })

        });

    });
    socket.on('message', function(data) {
        var dataFromClient = new message();
        message.findOne({}, function(err, doc) {
            if (err) {
                console.error('error, no entry found');
            } else {
                dataFromClient.nick = data["nick"];
                dataFromClient.content = data["content"];
                dataFromClient.isImage = data["isImage"];
            }
            dataFromClient.save(function(err) {
                if (err) {
                    console.error('error, no entry found');
                } else {
                    console.error('Saved');
                }
            });
        })

        var sockets = io.sockets.sockets;

        socket.broadcast.emit('message', data);
        console.log(data)
            //socket.broadcast.emit('message', data);
    })
    socket.on('disconnect', function() {
        console.log('one user disconnected ' + socket.id);
    })
})



http.listen(3000, function() {
    console.log('server listening on port 3000');
})